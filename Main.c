#define _CRT_SECURE_NO_WARNINGS
#include "stdio.h"
#include "stdlib.h"
#include <stdbool.h>


/*************** Board ***************/
#define ROWS 6
#define COLS 7
char board[ROWS][COLS];


/*********** Declarations ************/

/// This function initializes the game board by assigning each cell
/// with ' ' (resulting with an empty game board).
void initBoard();

/// This function gets a row number and a column number (a cell),
/// and returns the character in that cell (could be 'X', 'O' or ' ').
/// For example:
/// char c = getCell(1, 1);
char getCell(int row, int col);

/// This function gets a row number, a column number and a sign,
/// and assigns the cell with the given sign.
/// For example:
/// setCell(1, 1, 'X');
void setCell(int row, int col, char sign);

/// This function clears the screen.
void clearScreen();

//this function prints the game board
//gets: None
//returns: prints updated board
void printBoard();



//this function checks if the players input is a legal column number 
//if it's ilegal - it prints a message telling it's ilegal and asks to enter a new one. returns false
//if the input is legal and the column is available, returns true. 
//gets: player input column and integer from the counter of rowNumber 
//returns: true or false
bool checkInput(int colInput);


//the function checks if there are 4 for a win on the board
//gets: None
//returns: if there's a winner - returns true 			
//returns: if there's no winner yet - returns false 			
bool checkWin(int playerCol, int row);


//the function checks if there are no more available steps for the players
//gets: None
//returns: if the board is full - returns true
//returns: if the board isn't full  - returns false
bool boardIsFull();

//this function checks if the column is full.
//if the column is full - it prints a message telling it's full and returns false.
//gets: player column input 
//returns: true or false
bool isColFull(int playerCol);


//this function checks if there are 4 similar signs in a row and returns true if there are.
//gets: player column input and the row of the enterd sign
//gets: true or false
//returns: true or false
bool rowCheck(int playerCol, int row);


//this function checks if there are 4 straight similar signs in a col and returns true if there are.
//gets: player column input and the row of the enterd sign
//gets: true or false
//returns: true or false
bool topToBottom(int playerCol, int row);


//this function checks if there are 4 straight similar signs in a diagonal line from top left to
// right bottom and returns true if there are.
//gets: player column input and the row of the enterd sign
//gets: true or false
//returns: true or false
bool leftToRightD(int playerCol, int row);


//this function checks if there are 4 straight similar signs in a diagonal line from top right to
// left bottom and returns true if there are.
//gets: player column input and the row of the enterd sign
//gets: true or false
//returns: true or false
bool rightToLeftD(int playerCol, int row);




/*************** Main ****************/
void main() {
	int i;
	int playerCol;
	bool keepPlaying = true;
	int playerTurn = 1;
	int rowNumber[COLS] = { 7,7,7,7,7,7,7 };//array for rows of the inputs
	initBoard();
	while (keepPlaying) {
		if (boardIsFull()) {
			playerTurn = 1;
			for (i = 0; i < COLS; i++)
				rowNumber[i] = 7;
			initBoard();
		}
		printBoard();
		printf("Player number %d:\nPlease enter col input (a number between 1-7): ", playerTurn);
		scanf("%d", &playerCol);
		while ((!(checkInput(playerCol))) || (isColFull(playerCol))) {
			scanf("%d", &playerCol);
		}
		rowNumber[playerCol - 1] = rowNumber[playerCol - 1]--;
		if (playerTurn % 2 > 0) {
			setCell(rowNumber[playerCol - 1], playerCol, 'X');
			playerTurn = playerTurn++;
		}
		else {
			setCell(rowNumber[playerCol - 1], playerCol, 'O');
			playerTurn = playerTurn--;
		}
		if (checkWin(playerCol, rowNumber[playerCol - 1])) {
			if (playerTurn % 2 > 0) {
				playerTurn = playerTurn++;
			}
			else {
				playerTurn = playerTurn--;
			}
			keepPlaying = false;
		}
		else
			clearScreen();

	}
	printf("\nThe WINNER is Player %d", playerTurn);
}




/********** Implementations **********/


char getCell(int row, int col)
{
	return board[row - 1][col - 1];
}

void setCell(int row, int col, char sign)
{
	board[row - 1][col - 1] = sign;
}

void initBoard()
{
	int i, j;

	for (i = 0; i < ROWS; i++)
	{
		for (j = 0; j < COLS; j++)
		{
			setCell(i + 1, j + 1, ' ');
		}
	}
}

void clearScreen()
{
	system("cls");
}


void printBoard() {
	int col, row;
	char firstCol[7] = { 'A', 'B', 'C', 'D', 'E', 'F' };
	printf(" ");
	for (col = 0; col < COLS; col++) {
		printf("    %d", col + 1);
	}
	printf("\n");
	for (row = 0; row < ROWS; row++) {
		printf("%c", firstCol[row]);
		for (col = 0; col < COLS; col++) {
			printf("    %c", board[row][col]);
		}
		printf("\n");
	}
}


bool isColFull(int playerCol) {
	if (getCell(1, playerCol) != ' ') {
		printf("The column you chose is full, Please choose another one between 1-7: ");
		return true;
	}
	else
		return false;
}


bool checkInput(int colInput) {
	if ((colInput > 7) || (colInput < 1)) {
		printf("Please enter an input only between 1-7: ");
		return false;
	}
	else
		return true;
}



bool checkWin(int playerCol, int row) {
	if (rowCheck(playerCol, row) || topToBottom(playerCol, row) ||
		leftToRightD(playerCol, row) || rightToLeftD(playerCol, row))
		return true;
	else
		return false;
}






bool topToBottom(int playerCol, int row) {
	int i;
	int tempRow = row + 1;
	if (row == 1 || row == 2 || row == 3) {
		for (i = 0; i < 4; i++) {
			if (getCell(tempRow, playerCol) == getCell(row, playerCol)) {
				tempRow--;
			}
			else
				return false;
		}
		return true;
	}
	else
		return false;
}


bool rowCheck(int playerCol, int row) {
	int i;
	int counter = 1;
	int rightTempCol = playerCol + 1;
	int leftTempCol = playerCol - 1;
	for (i = 0; i < 4; i++) {
		if (rightTempCol > 7)
			break;
		if (getCell(row, rightTempCol) == getCell(row, playerCol)) {
			rightTempCol++;
			counter++;
		}
		else
			break;
		if (counter == 4)
			return true;
	}
	for (i = 0; i < 4; i++) {
		if (leftTempCol < 1)
			break;
		if (getCell(row, leftTempCol) == getCell(row, playerCol)) {
			leftTempCol--;
			counter++;
		}
		else
			break;
		if (counter == 4)
			return true;
	}
	if (counter == 4)
		return true;
	else
		return false;
}


bool boardIsFull() {
	int i;
	for (i = 0; i <= COLS; i++) {
		if (getCell(1, i) == ' ')
			return false;
	}
	clearScreen();
	return true;
}


bool rightToLeftD(int playerCol, int row) {
	int i;
	int counter = 1;
	int tempColRight = playerCol + 1;
	int tempColLeft = playerCol - 1;
	int tempRowRight = row - 1;
	int tempRowLeft = row + 1;
	for (i = 0; i < 4; i++) {
		if (tempColRight > 7 || tempRowRight < 1)
			break;
		if (getCell(tempRowRight, tempColRight) == getCell(row, playerCol)) {
			counter++;
			tempColRight++;
			tempRowLeft--;
		}
		else
			break;
	}
	for (i = 0; i < 4; i++) {
		if (tempColLeft < 1 || tempRowLeft > 6)
			break;
		if (getCell(tempRowLeft, tempColLeft) == getCell(row, playerCol)) {
			counter++;
			tempColLeft--;
			tempRowLeft++;
		}
		else
			break;
	}
	if (counter == 4)
		return true;
	else
		return false;
}




bool leftToRightD(int playerCol, int row) {
	int i;
	int counter = 1;
	int tempColRight = playerCol + 1;
	int tempColLeft = playerCol - 1;
	int tempRowRight = row + 1;
	int tempRowLeft = row - 1;
	for (i = 0; i < 4; i++) {
		if (tempColLeft < 1 || tempRowLeft < 1)
			break;
		if (getCell(tempRowLeft, tempColLeft) == getCell(row, playerCol)) {
			counter++;
			tempRowLeft++;
			tempColLeft++;
		}
		else
			break;
	}
	for (i = 0; i < 4; i++) {
		if (tempColRight > 7 || tempRowRight > 6)
			break;
		if (getCell(tempRowRight, tempColRight) == getCell(row, playerCol)) {
			counter++;
			tempColRight++;
			tempRowRight++;
		}
		else
			break;
	}
	if (counter == 4)
		return true;
	else
		return false;
}